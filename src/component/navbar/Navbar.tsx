import React from 'react'
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {logout} from "../../services/authentication";
import {Link} from "react-router-dom";


export default () => {

    const handleLogout = async () => {
        logout()
    }

    return (
        <Navbar expand="lg" bg="primary" fixed="top" variant="dark">
            <Link to="/">Family Memories</Link>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    <Link to="/recette">Recette</Link>
                </Nav>
                <Nav >
                    <Nav.Link onClick={handleLogout} title="Logout"><ExitToAppIcon /></Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}