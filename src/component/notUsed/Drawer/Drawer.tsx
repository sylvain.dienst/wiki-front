import React from 'react'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import HomeIcon from '@material-ui/icons/Home';
import ListItemLink from '../ListItemLink/ListItemLink';

const drawerWidth = 200;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
        },
        drawerPaper: {
            width: drawerWidth,
        },
        toolbar: theme.mixins.toolbar,
    }),
);

export default () => {
    const classes = useStyles()

    return (
        <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
                paper: classes.drawerPaper,
            }}>
            <div className={classes.toolbar} />
            <List>
                <ListItemLink to="/" icon={<HomeIcon />} primary="Home" />
            </List>
        </Drawer>
    )
}