import React, {useState, ChangeEvent} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { post } from '../../services/api';
import * as hasher from 'password-hash';
import {AxiosResponse} from "axios";
import InputText from "../../component/form/input/InputText";
import Form from "../../component/form/Form";
import CssBaseline from "@material-ui/core/CssBaseline";
import {Avatar} from "@material-ui/core";
import Logo from "../../img/logo.png";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  login: {
    textAlign: 'center',
  },
}));


export default () => {
  const [ email, setEmail ] = useState('')
  const [ password, setPassword ] = useState('')
  const [ emailError, setEmailError ] = useState('')
  const [ bottomError, setBottomError ] = useState('')

  const [loading, setLoading ] = useState()

  const classes = useStyles();
  const handleNameChange = (evt : ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setEmail(evt.target.value)
  }

  const handlePasswordChange = (evt : ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setPassword(evt.target.value)
  }

  const handleSubmitLogin = async (evt: any) => {
    evt.preventDefault();
    setLoading(true)
    setEmailError('')
    setBottomError('')
    if (email.length === 0) {
      setEmailError("Email manquant")
    } else if (password.length === 0) {
      setBottomError('Password manquant')
    } else {
      post('auth/login', {
        email
      }).then((res: AxiosResponse) => {
        setLoading(false)
        if (res.status === 400) {
          setBottomError("Email invalide")
        } else {
          if (!hasher.verify(password, res.data.password)) {
            setBottomError('Mot de passe invalide')
          } else {
            localStorage.setItem('currentUser', JSON.stringify(res.data));
            window.location.replace('/')
          }
        }
      }).catch(() => {
        setBottomError("Un probleme au niveau du serveur est survenu...")
        setLoading(false)
      });
    }
  };

  return (
      <Container component="main" maxWidth="xs" className={classes.login}>
        <CssBaseline />
          <img src={Logo}  alt="Logo" className={classes.login}/>
        <Form titleForm="Connexion" handleSubmit={handleSubmitLogin} loading={loading} buttonValue="Connexion">
              <InputText type="email"    value={email}    error={emailError}  handleChange={handleNameChange}     label="Adresse mail" name="email"    required />
              <InputText type="password" value={password} error={bottomError} handleChange={handlePasswordChange} label="Mot de passe" name="password" required={true}/>
        </Form>
    </Container>
  );
}