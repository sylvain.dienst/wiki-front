import React, {useState, ChangeEvent, useEffect} from 'react';
import InputText from "../../component/form/input/InputText";
import {createStyles, InputLabel, makeStyles, Theme, Typography} from "@material-ui/core";
import Form from "../../component/form/Form";
import Select from "../../component/form/input/Select";
import Grid from "@material-ui/core/Grid";
import MainContainer from "../MainContainer";
import Paper from "@material-ui/core/Paper";
import AddBoxIcon from '@material-ui/icons/AddBox';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },
    }),
);

export default () => {
    const classes = useStyles()
    const [recipeName, setRecipeName] = useState('')
    const [recipeNameError, setRecipeNameError] = useState('')
    const [loading, setLoading ] = useState()

    const [category, setCategory] = useState('Entree')
    const [avatar, setAvatar] = useState('')



    const handleRecipeNameChange =(event: any) => {
        setRecipeName(event.target.value);
    }
    const handleCategoryChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setCategory(event.target.value as string);
        setAvatar("/src/img/entree.jpg");
    };

    const handleSubmit = () => {
    }
    const options = [
        {
            label: "Entree",
            value: 1
        },
        {
            label: "Plat",
            value: 2
        },
        {
            label: "Dessert",
            value: 3
        }
    ]
    return (
        <MainContainer>
            <Grid container spacing={10}>
                <Grid item xs={3}>
                   <Form titleForm="Ajouter une recette" handleSubmit={handleSubmit} loading={loading} buttonValue="Enregistrer" >
                       <InputText handleChange={handleRecipeNameChange} value={recipeName} required error={recipeNameError} label="Nom de la recette" name="recipeName" type="text" />
                       <Select value={category} handleChange={handleCategoryChange} options={options} label="Categorie" />
                       <AddBoxIcon />
                   </Form>
                </Grid>
                <Grid item xs>
                    <Paper className={classes.paper}>
                        <Typography variant="h5">{recipeName}</Typography>
                        <Typography variant="body1">

                        </Typography>
                    </Paper>
                </Grid>
            </Grid>

        </MainContainer>
    )
}