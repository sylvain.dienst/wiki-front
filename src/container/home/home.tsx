import React, {ChangeEvent, useState} from "react";
import {getCurrentUser, setCurrentUser} from "../../services/authentication"
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { post } from "../../services/api"
import {AxiosResponse} from "axios";
import * as hasher from 'password-hash'

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default () => {
    const [ pwd1, setpwd1 ] = useState('')
    const [ pwd2, setpwd2 ] = useState('')
    const [ pwd1Error, setPwd1Error] = useState('')
    const [ pwd2Error, setPwd2Error] = useState('')
    const classes = useStyles();

    const handlePwd1Change = (evt : ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setpwd1(evt.target.value)
    }

    const handlePwd2Change = (evt : ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setpwd2(evt.target.value)
    }

    const handleSubmit = async (evt :any) => {
        evt.preventDefault();
        setPwd1Error('')
        setPwd2Error('')
        if (pwd1.length === 0) {
            setPwd1Error("Mot de passe manquant")
        } else if (pwd2.length === 0) {
            setPwd2Error("Mot de passe manquant")
        }else if (pwd1 != pwd2) {
            setPwd2Error("Les deux mots de passe sont different")
        } else {
            post('auth/changePassword', {
                pwd1: hasher.generate(pwd1),
                id: getCurrentUser().id
            }).then((res: AxiosResponse) => {
                let user = getCurrentUser()
                user.validPwd = true
                setCurrentUser(user)
                window.location.replace('/')
            })
        }
    }
    let render = (<div>Site en construction</div>)
    if (!getCurrentUser().validPwd) {
        render = (
            <div>
                Bienvenue {getCurrentUser().firstName} !
                <Container component="main" maxWidth="xs">
                    <CssBaseline />
                    <Typography component="h1" variant="h5">
                        Creation du nouveau mot de passe
                    </Typography>
                    <form className={classes.form} >
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="pwd1"
                            label="Mot de passe"
                            type="password"
                            name="pwd1"
                            autoFocus
                            value={pwd1}
                            helperText={pwd1Error}
                            error={pwd1Error.length > 0}
                            onChange={handlePwd1Change}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="pwd2"
                            label="Retape le mot de passe"
                            type="password"
                            id="pwd2"
                            value={pwd2}
                            error={pwd2Error.length > 0}
                            helperText={pwd2Error}
                            onChange={handlePwd2Change}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={handleSubmit}
                        >
                            Valider
                        </Button>
                    </form>
                </Container>
            </div>
        )
    }
    return render;
}