export const getCurrentUser = () => JSON.parse(localStorage.getItem('currentUser') as string);

export const setCurrentUser = (user: Object) => {
    localStorage.setItem('currentUser', JSON.stringify(user));
}

export const logout = () => {
    localStorage.removeItem('currentUser');
    window.location.replace('/login')
}
