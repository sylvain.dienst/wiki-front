import {getCurrentUser} from "./authentication";

export default () => getCurrentUser() && getCurrentUser().token ? getCurrentUser().token : {}